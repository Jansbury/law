<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>The Law Lounge | Solicitors & Realtors</title>
        <meta name="description" content="The Law Lounge | Solicitors & Realtors">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        
        <header>
        <div class="cntr clearfix">
        <div class="logoCon">
	        <img src="i/logo.svg" alt="logo"/>        
	       </div>
	        <nav class="main">

							<ul class="group" id="example-one">
					        <li class="active"><a href="#">About</a></li>
					        <li><a href="#">Services</a></li>
					        <li><a href="#">Blog</a></li>
					        <li><a href="#">Contact</a></li>
					        <li><a href="#">Realestate</a></li>
					    </ul>

					   
	        </nav>
	        
        </div>
        <div class="line"></div>
        </header>