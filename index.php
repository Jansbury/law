<?php include('header.php'); ?>

		    <div class="welcomeImgCon">
			    <img src="i/welcome-demo-01.jpg" alt="welcome"/>
			    
		    </div>
		    <div class="cntr">  
		        <div class="servCon clearfix">
			      	<div class="serv1">
				      	<h2>Family Law</h2>
				      	<p>Relocation, Guardianship,<br />
								Child Youth & Family Matters,<br />
								Child Support
								</p>
							<div class="view"><a href="#">View All</a></div>
			      	</div>
			      	<div class="serv2">
				      	<h2>Estate Planning</h2>
				      	<p>Wills, Trusts, Estate Administration,<br />
									Power Of Attorney/Enduring,<br />
									Power of Attorney
								</p>
							<div class="view"><a href="#">View All</a></div>
			      	</div>
			      	<div class="serv3">
				      	<h2>Property</h2>
				      	<p>Buying or selling a home 
								Refinancing</p>
							<div class="view"><a href="#">View All</a></div>
			      	</div>
			      	<div class="serv4">
				      	<h2>Commercial</h2>
				      	<p>Buying or selling a business<br />
										Leases<br />
										Company formation</p>
								<div class="view"><a href="#">View All</a></div>
			      	</div>
			      	<div class="serv5">
				      	<h2>Consultancy Service</h2>
				      	<p>We offer 50 years' combined <br />
										experience in separation, custody <br />
										and matrimonial property.</p>
							<div class="view"><a href="#">View All</a></div>
			      	</div>
			      		<div class="serv6">
				      	<h2>Other</h2>
				      	<p>Employment Law<br />
									Elder Law<br />
									Maori Land disputes</p>
									<div class="view"><a href="#">View All</a></div>
			      	</div>
			      				        
		        </div>
		        
	        </div> <?php// END .cntr ?>
	        
<?php include('footer.php'); ?>
