	        <footer class="clearfix">
		        <div class="cntr clearfix">
			        <div class="fCol1">
				        <h3>Contact us</h3>
				        	<span>mail@thelawlounge.co.nz</span>
					        <span>(+64) 9 551 6120</span>
			        </div>
			        <div class="fCol2">
				        <h3>Address</h3>
				        	<div class="addleft">
					        	<span>Level 2, 17 Anzac Street Takapuna,</span>
						        <span>Auckland, 0622</span>
					        </div>
					        <div class="addright">
					        	<span>PO Box 33 241 Takapuna,</span>
						        <span>Auckland, 0740</span>
					        </div>
			        </div>
			        <div class="fCol3">
				        <h3>Terms</h3>
				        <span><a href="#" target="_blank">Terms of Engagement</a></span>
					      <span><a href="#" target="_blank">Terms & Conditions</a></span>
				        
			        </div>
			        <div class="fCol4">
				        <h3>connect</h3>
									<ul>
										<li class="facebook"><a href="#">Facebook</a></li>
										<li class="linkedin"><a href="#">Linked In</a></li>
									</ul>
									<div class="copy">&copy; <span id="year">&nbsp;</span> The Law Lounge</div>
					      
				        
			        </div>
		        </div>
		        
	        </footer>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>


       <!--
 <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
-->
    </body>
</html>